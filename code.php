<?php 


// While Loop
function whileLoop() {
	$count = 5;

	while($count !== 0) {
		echo $count.'<br/>';
		$count--;
	};
};


// Do-While Loop
function doWhileLoop() {
	$count = 20;

	do {
		echo $count.'<br/>';
		$count--;
	} while ($count > 0);
};


// For Loop
function forLoop() {
	for ($count = 0; $count <= 20; $count++) {
		echo $count.'<br/>';
	};
};


// Continue and break statements
function modifiedForLoop() {
	for ($count = 0; $count <= 20; $count++) {
		if ($count % 2 === 0) {
			continue;
		}
		echo $count.'<br/>';
		if ($count > 10) {
			break;
		}
	};
};


// Array Manipulation

$studentNumbers = array('2020-1923', '2020-1924', '2020-1925');
// New feature in the version of PHP v. of 5.4
$newStudentNumbers = ['2022-1111', '2022-1112', '2022-1113'];

// Simple Arrays
$tasks  = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake sass'
];


// Associative Array
$gradePeriods = [
	'firstGrading' => 99.5,
	'secondGrading' => 94.3,
	'thirdGrading' => 90,
	'fourthGrading' => 97
];

// Two-Dimensional Array
$heroes = [
	['Iron Man', 'Thor', 'Hulk'],
	['Wolverine', 'Cyclops', 'Jean', 'Grey'],
	['Batman', 'Superman', 'Wonderwoman']
];

// Array Iterative Method: foreach()

foreach($heroes as $hero) {
	echo $hero;
}

// Array Sorting
$computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

$sortedBrands = $computerBrands;
$reverseSortedBrands = $computerBrands;

sort($sortedBrands);
rsort($reverseSortedBrands);


// Other Array Functions

function searchBrand($brands, $brand) {
		return (in_array($brand, $brands)
	) ? "$brand is in the array."
		: "$brand is not in the array.";
};

$reversedGradesPeriods = 
	array_reverse($gradePeriods);

// Activity 1
function divisibilityFunc(){
	for($count = 0; $count <=1000; $count++){
		if($count % 5 !== 0){
			continue;
		}
		echo $count.', ';
		if($count>1000){
			break;
		}
	}
}

//Activity 2

$arrayStudents = [];


?>