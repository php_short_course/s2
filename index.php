<?php require_once "./code.php"; ?>
<!DOCTYPE html>
<html>
<head>
	<title>s2: Repetition Control Structures and Array Manipulation</title>
</head>
<body>

	<h1>Repetition Control Structures</h1>
	<h4>While Loop</h4>
	<?php whileLoop(); ?>
	<h4>Do-While Loop</h4>
	<?php doWhileLoop(); ?>
	<h4>For Loop</h4>
	<?php forLoop(); ?>

	<h4>Continue and Break Statements</h4>
	<?php modifiedForLoop(); ?>

	<h4>Associative Array</h4>
	<?php echo $gradePeriods -> firstGrading; ?>

	<ul>
		<?php foreach($gradePeriods as $period => $grade) { 
	// <?= is the shortcut for <?php  echo ?>

	<li>Grade in <?= $period ?> is <?= $grade ?></li>
<?php } ?>
</ul>


<h4>2-Dimensional Array</h4>
<?php echo $heroes[2][1]; ?>

<ul>
	<?php 
	foreach($heroes as $team) {
		forEach($team as $member){
			?> 
			<li><?php echo $member ?></li>
			<?php
		}
	}
	?>
</ul>

<pre>
	<h2>Sorting</h2>
	<?php print_r($sortedBrands); ?>		
</pre>

<pre>
	<h2>Reverse Sorting</h2>
	<?php print_r($reverseSortedBrands); ?>		
</pre>

<h2>Array Mutations (Append)</h2>
<?php array_push($computerBrands, 'Apple'); ?>


<p>
	<?php echo searchBrand($computerBrands, 'Asus') ?>
</p>


<pre>
	<?php print_r($reversedGradesPeriods); ?>
</pre>

<!-- Activity 1 -->

<h1>Divisibles of Five</h1>
<?php divisibilityFunc();?><br/>

<!-- Activity 2 -->

<h1>Array Manipulation</h1>
<?php array_push($arrayStudents,'John Smith') ?>
<pre><?php var_dump($arrayStudents); ?></pre>
<pre><?php echo count($arrayStudents); ?></pre>
<?php array_push($arrayStudents,'Jane Smith') ?>
<pre><?php var_dump($arrayStudents); ?></pre>
<pre><?php echo count($arrayStudents); ?></pre>
<?php array_shift($arrayStudents) ?>
<pre><?php var_dump($arrayStudents); ?></pre>
<pre><?php echo count($arrayStudents); ?></pre>





</body>
</html>